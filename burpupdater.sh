#!/bin/bash 

# Automated script to replace Burp community to burp pro everytime it gets over written by kali updates. 
# As Kali burp community updates Don't always happen as a new release is out this script will pull down the new version


echo "--------------------------------------------------------------------------------"
echo "                    Kali Linux BurpSuite Script                                "
echo "--------------------------------------------------------------------------------"
echo "written by Tribal Phoenix                                                      "   
echo "--------------------------------------------------------------------------------"
echo "                                OPTIONS:-                                       "
echo "1.Update Burp Community   2. Update Burp Pro   3.Replace Burp Community with Pro "                                                                        
echo "--------------------------------------------------------------------------------"

echo "" 

read -p "Enter Your Option: " selectvar
#read selectvar 


if [ $selectvar = '1' ]; then
     # This is useful for when the newest community isn't availible in Kali repos yet!
     read -p "Enter the Version Number You Want To Update Too: " version
     echo ""

     if [ -f "/usr/bin/burpsuite" ]; then
         rm /usr/bin/burpsuite
     fi 

     wget -O burpsuite 'https://portswigger.net/burp/releases/download?product=community&version='$version'&type=jar'
     #while [ ! -f burpsuite ]; do sleep 1; done 
     echo "File Successfully Downloaded"
     mv burpsuite /usr/bin 
     echo "Finished Burpsuite Is Updated"

fi


if [ $selectvar == '2' ]; then 
     # This will allow updates to pro not completed yet:)
     echo "pro"
     echo "Finished Burpsuite Is Updated"
fi


if [ $selectvar == '3' ]; then
     # This is useful for when kali overwrites pro with Community during updates.
     echo "Replacing Community with Pro"

     if [ -f "/usr/bin/burpsuite" ]; then
         rm /usr/bin/burpsuite
     fi 

     if [ ! -f "/root/Downloads/burp-backup/burpsuite" ]; then
         echo "" 
         echo "You Don't Seem To Have A Backup!"
         echo "Create A directory Called burp-backup In Downloads then Download Burp Pro Into that Directory rename It As burpsuite"
         exit
     fi 


     cp /root/Downloads/burp-backup/burpsuite /usr/bin
     echo "Finished"
fi 

exit